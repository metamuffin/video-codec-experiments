pub type UVec2 = Vec2<usize>;

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Vec2<T> {
    pub x: T,
    pub y: T,
}

impl From<Vec2<isize>> for Vec2<u16> {
    fn from(value: Vec2<isize>) -> Self {
        Self {
            x: value.x as u16,
            y: value.y as u16,
        }
    }
}
impl From<Vec2<u16>> for Vec2<isize> {
    fn from(value: Vec2<u16>) -> Self {
        Self {
            x: value.x as isize,
            y: value.y as isize,
        }
    }
}
impl From<Vec2<f32>> for Vec2<isize> {
    fn from(value: Vec2<f32>) -> Self {
        Self {
            x: value.x as isize,
            y: value.y as isize,
        }
    }
}

impl Vec2<isize> {
    pub const ZERO: Vec2<isize> = Vec2 { x: 0, y: 0 };
    pub const UP: Vec2<isize> = Vec2 { x: 0, y: -1 };
    pub const LEFT: Vec2<isize> = Vec2 { x: -1, y: 0 };
}
impl Vec2<f32> {
    pub const ZERO: Vec2<f32> = Vec2 { x: 0.0, y: 0.0 };
    pub const UP: Vec2<f32> = Vec2 { x: 0.0, y: -1.0 };
    pub const LEFT: Vec2<f32> = Vec2 { x: -1.0, y: 0.0 };
}

impl<T: std::ops::Div<Output = T> + Copy> Vec2<T> {
    pub fn downscale(&self, f: T) -> Self {
        Self {
            x: self.x / f,
            y: self.y / f,
        }
    }
}

impl<T: std::ops::Mul<Output = T> + Copy> Vec2<T> {
    pub fn scale(&self, f: T) -> Self {
        Self {
            x: self.x * f,
            y: self.y * f,
        }
    }
    pub fn area(&self) -> T {
        self.x * self.y
    }
}

impl Vec2<isize> {
    pub fn x_only(&self) -> Self {
        Self { x: self.x, y: 0 }
    }
    pub fn y_only(&self) -> Self {
        Self { x: 0, y: self.y }
    }
}

impl Into<Vec2<f32>> for Vec2<isize> {
    fn into(self) -> Vec2<f32> {
        Vec2 {
            x: self.x as f32,
            y: self.y as f32,
        }
    }
}
impl From<(isize, isize)> for Vec2<f32> {
    fn from((x, y): (isize, isize)) -> Self {
        Vec2 {
            x: x as f32,
            y: y as f32,
        }
    }
}

impl<T: std::ops::Add> std::ops::Add for Vec2<T> {
    type Output = Vec2<T::Output>;
    #[inline]
    fn add(self, rhs: Self) -> Self::Output {
        Vec2 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}
impl<T: std::ops::Sub> std::ops::Sub for Vec2<T> {
    type Output = Vec2<T::Output>;
    #[inline]
    fn sub(self, rhs: Self) -> Self::Output {
        Vec2 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}
impl<T: std::ops::Mul> std::ops::Mul for Vec2<T> {
    type Output = Vec2<T::Output>;
    #[inline]
    fn mul(self, rhs: Self) -> Self::Output {
        Vec2 {
            x: self.x * rhs.x,
            y: self.y * rhs.y,
        }
    }
}

impl<T> From<(T, T)> for Vec2<T> {
    #[inline]
    fn from((x, y): (T, T)) -> Self {
        Vec2 { x, y }
    }
}
