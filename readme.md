# Experimental Video Codecs

My experiments on lossy video compression.

## `bv1`

A somewhat minimal video codec that is able to achieve quality comparable to
H.264.

## Other codecs

- `evc` is my first proper attempt at video compression. features motion
  compensation and broken dct.
- `vgcodec` approximates images by drawing circles (on the GPU).
- (`dhwt-codec` tries to compress using a discrete haar wavelet across all three
  dimensions. that doesnt work well)

## the test framework

| Variable   | Description  |
| ---------- | ------------ |
| `V_WIDTH`  | Video width  |
| `V_HEIGHT` | Video height |
