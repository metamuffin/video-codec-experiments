use bincode::config;
use std::{
    fs::File,
    io::{BufReader, BufWriter, Read, Write},
};

pub type Value = f32;
pub type VideoBuf = Vec<Vec<Vec<Value>>>;
pub const ZERO: Value = 0 as Value;
pub const TWO: Value = 2 as Value;

pub fn empty_videobuf(x: usize, y: usize, z: usize) -> VideoBuf {
    (0..x)
        .map(|_| (0..y).map(|_| (0..z).map(|_| ZERO).collect()).collect())
        .collect()
}

pub fn outfile(p: &str) -> impl Write {
    BufWriter::new(File::create(p).unwrap())
}
pub fn infile(p: &str) -> impl Read {
    BufReader::new(File::open(p).unwrap())
}

pub fn read_videobuf(f: &mut impl Read) -> VideoBuf {
    bincode::decode_from_std_read(f, config::standard()).unwrap()
}
pub fn write_videobuf(f: &mut impl Write, i: VideoBuf) {
    bincode::encode_into_std_write(i, f, config::standard()).unwrap();
}

pub fn write_videobuf_small(f: &mut impl Write, mut i: VideoBuf) {
    for _ in 0..(i.len() / 2) {
        i.pop();
    }
    for i in &mut i {
        for _ in 0..(i.len() / 2) {
            i.pop();
        }
        for i in i {
            for _ in 0..(i.len() / 2) {
                i.pop();
            }
        }
    }
    write_videobuf(f, i);
}
pub fn read_videobuf_small(f: &mut impl Read) -> VideoBuf {
    let mut i = read_videobuf(f);

    for i in &mut i {
        for i in i {
            for _ in 0..i.len() {
                i.push(ZERO);
            }
        }
    }
    for i in &mut i {
        for _ in 0..i.len() {
            i.push((0..i[0].len()).map(|_| ZERO).collect());
        }
    }
    for _ in 0..i.len() {
        i.push(
            (0..i[0].len())
                .map(|_| ((0..i[0][0].len()).map(|_| ZERO)).collect())
                .collect(),
        );
    }
    i
}
