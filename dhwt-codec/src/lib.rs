use clap::Parser;

pub mod io;
pub mod transform;
pub mod trim;
pub mod view;

#[derive(Parser)]
#[clap(about)]
pub struct CommonArgs {
    #[arg(short)]
    pub x: usize,
    #[arg(short)]
    pub y: usize,
    #[arg(short)]
    pub z: usize,

    #[arg(short, long, default_value = "3")]
    pub channels: usize,

    pub infile: String,
    pub outfile: String,
}
