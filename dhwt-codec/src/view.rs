use std::ops::{Index, IndexMut};

use crate::io::{VideoBuf, Value};

#[derive(Copy, Clone, Debug)]
pub enum IndexMode {
    XY(usize, usize),
    XZ(usize, usize),
    YZ(usize, usize),
}

pub struct BufferView<'a> {
    mode: IndexMode,
    buf: &'a mut VideoBuf,
}

impl<'a> BufferView<'a> {
    pub fn new(buf: &'a mut VideoBuf, mode: IndexMode) -> Self {
        BufferView { mode, buf }
    }
}

impl Index<usize> for BufferView<'_> {
    type Output = Value;

    fn index(&self, a: usize) -> &Self::Output {
        match self.mode {
            IndexMode::XY(x, y) => &self.buf[x][y][a],
            IndexMode::XZ(x, z) => &self.buf[x][a][z],
            IndexMode::YZ(y, z) => &self.buf[a][y][z],
        }
    }
}
impl IndexMut<usize> for BufferView<'_> {
    fn index_mut(&mut self, a: usize) -> &mut Self::Output {
        match self.mode {
            IndexMode::XY(x, y) => &mut self.buf[x][y][a],
            IndexMode::XZ(x, z) => &mut self.buf[x][a][z],
            IndexMode::YZ(y, z) => &mut self.buf[a][y][z],
        }
    }
}
