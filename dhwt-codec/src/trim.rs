use crate::io::{Value, TWO, ZERO};
use std::ops::{Index, IndexMut};

pub fn trim<X: Index<usize, Output = Value> + IndexMut<usize, Output = Value>>(
    size: usize,
    a: &mut X,
) {
    let half = size / 2;
    let quarter = size / 4;
    for i in 0..(size / 2 / 4) {
        let hi = half + i * 4;
        let qi = quarter + i * 2;
        a[qi] = (a[qi + 0] + a[qi + 1]) / TWO;
        a[qi + 1] = (a[hi + 0] + a[hi + 1] + a[hi + 2] + a[hi + 3]) / (TWO * TWO);
    }
    for i in half..size {
        a[i] = ZERO;
    }
}

pub fn untrim<X: Index<usize, Output = Value> + IndexMut<usize, Output = Value>>(
    size: usize,
    a: &mut X,
) {
    let half = size / 2;
    let quarter = size / 4;
    for i in 0..(size / 2 / 4) {
        let hi = half + i * 4;
        let qi = quarter + i * 2;
        a[hi + 0] = a[qi + 1];
        a[hi + 1] = a[qi + 1];
        a[hi + 2] = a[qi + 1];
        a[hi + 3] = a[qi + 1];
        a[qi + 1] = a[qi];
    }
}
