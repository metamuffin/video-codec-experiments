use clap::Parser;
use dhwt_codec::io::{outfile, write_videobuf, Value};
use std::io::{stdin, Read};

#[derive(Parser)]
#[clap(about)]
struct ImportArgs {
    #[arg(short)]
    x: usize,
    #[arg(short)]
    y: usize,
    #[arg(short)]
    z: usize,

    #[arg(short, long, default_value = "3")]
    channels: usize,

    outfile: String,
}

fn main() {
    let args = ImportArgs::parse();

    let mut rawbuf = (0..(args.x * args.y * args.z * args.channels))
        .map(|_| 0u8)
        .collect::<Vec<_>>();
    stdin().read_exact(&mut rawbuf).unwrap();

    let mut o = outfile(&args.outfile);

    for c in 0..args.channels {
        let mut cols = vec![];
        for x in 0..args.x {
            let mut col = vec![];
            for y in 0..args.y {
                let mut span = vec![];
                for z in 0..args.z {
                    span.push(
                        rawbuf[c
                            + (x * args.channels)
                            + (y * args.channels * args.x)
                            + (z * args.channels * args.x * args.y)] as Value,
                    );
                }
                col.push(span);
            }
            cols.push(col)
        }
        write_videobuf(&mut o, cols)
    }
}
