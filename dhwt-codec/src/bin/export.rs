use clap::Parser;
use dhwt_codec::io::{infile, read_videobuf};
use std::io::{stdout, BufWriter, Write};

#[derive(Parser)]
#[clap(about)]
struct ExportArgs {
    #[arg(short)]
    x: usize,
    #[arg(short)]
    y: usize,
    #[arg(short)]
    z: usize,

    #[arg(short, long, default_value = "3")]
    channels: usize,

    infile: String,
}

fn main() {
    let args = ExportArgs::parse();

    let mut i = infile(&args.infile);
    let mut writer = BufWriter::new(stdout());

    let mut channels = vec![];
    for _ in 0..args.channels {
        channels.push(read_videobuf(&mut i))
    }

    for z in 0..args.z {
        for y in 0..args.y {
            for x in 0..args.x {
                for c in 0..args.channels {
                    writer.write_all(&[channels[c][x][y][z] as u8]).unwrap();
                }
            }
        }
    }
    writer.flush().unwrap();
}
