use clap::Parser;
use dhwt_codec::{
    io::{empty_videobuf, infile, outfile, read_videobuf_small, write_videobuf, VideoBuf},
    transform, trim,
    view::{BufferView, IndexMode},
    CommonArgs,
};
use rayon::prelude::{IntoParallelIterator, ParallelIterator};

fn main() {
    let args = CommonArgs::parse();

    let mut inf = infile(&args.infile);
    let mut of = outfile(&args.outfile);

    for c in 0..args.channels {
        eprintln!("encoding channel #{c}");
        let a = empty_videobuf(args.x, args.y, args.z);
        let b = read_videobuf_small(&mut inf);

        eprintln!("\tdecoding Z");
        (0..args.x).into_par_iter().for_each(|x| {
            for y in 0..args.y {
                run_mode(make_mut(&b), make_mut(&a), IndexMode::XY(x, y), args.z)
            }
        });
        eprintln!("\tdecoding Y");
        (0..args.x).into_par_iter().for_each(|x| {
            for z in 0..args.z {
                run_mode(make_mut(&a), make_mut(&b), IndexMode::XZ(x, z), args.y)
            }
        });
        eprintln!("\tdecoding X");
        (0..args.y).into_par_iter().for_each(|y| {
            for z in 0..args.z {
                run_mode(make_mut(&b), make_mut(&a), IndexMode::YZ(y, z), args.x)
            }
        });
        write_videobuf(&mut of, a);
    }
}

fn run_mode(a: &mut VideoBuf, b: &mut VideoBuf, mode: IndexMode, size: usize) {
    trim::untrim(size, &mut BufferView::new(b, mode));
    transform::decode(
        size,
        &mut BufferView::new(a, mode),
        &mut BufferView::new(b, mode),
    );
}

fn make_mut<T>(r: &T) -> &mut T {
    #[allow(mutable_transmutes)]
    unsafe {
        std::mem::transmute::<&T, &mut T>(r)
    }
}
