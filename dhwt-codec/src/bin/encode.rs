use clap::Parser;
use dhwt_codec::{
    io::{empty_videobuf, infile, outfile, read_videobuf, write_videobuf_small, VideoBuf},
    transform, trim,
    view::{BufferView, IndexMode},
    CommonArgs,
};
use rayon::prelude::{IntoParallelIterator, ParallelIterator};

fn main() {
    let args = CommonArgs::parse();

    let mut inf = infile(&args.infile);
    let mut of = outfile(&args.outfile);

    for c in 0..args.channels {
        eprintln!("encoding channel #{c}");
        let o = empty_videobuf(args.x, args.y, args.z);
        let i = read_videobuf(&mut inf);

        eprintln!("\tencoding X");
        (0..args.y).into_par_iter().for_each(|y| {
            for z in 0..args.z {
                run_mode(make_mut(&i), make_mut(&o), IndexMode::YZ(y, z), args.x)
            }
        });
        eprintln!("\tencoding Y");
        (0..args.x).into_par_iter().for_each(|x| {
            for z in 0..args.z {
                run_mode(make_mut(&o), make_mut(&i), IndexMode::XZ(x, z), args.y)
            }
        });
        eprintln!("\tencoding Z");
        (0..args.x).into_par_iter().for_each(|x| {
            for y in 0..args.y {
                run_mode(make_mut(&i), make_mut(&o), IndexMode::XY(x, y), args.z)
            }
        });
        write_videobuf_small(&mut of, o);
    }
}

fn run_mode(a: &mut VideoBuf, b: &mut VideoBuf, mode: IndexMode, size: usize) {
    transform::encode(
        size,
        &mut BufferView::new(a, mode),
        &mut BufferView::new(b, mode),
    );
    trim::trim(size, &mut BufferView::new(b, mode));
}

fn make_mut<T>(r: &T) -> &mut T {
    #[allow(mutable_transmutes)]
    unsafe {
        std::mem::transmute::<&T, &mut T>(r)
    }
}
