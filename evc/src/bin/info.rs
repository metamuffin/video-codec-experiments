use anyhow::Context;
use libreschmux::format::{header::Header, ser::Source};
use std::io::BufReader;

fn main() {
    env_logger::init_from_env("LOG");
    let mut input = BufReader::new(std::io::stdin());
    let header = input.get::<Header>().context("reading header").unwrap();
    eprintln!("{header:#?}")
}
