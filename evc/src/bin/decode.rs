#![feature(box_patterns)]
use anyhow::Context;
use clap::Parser;
use indicatif::ProgressBar;
use libreschmux::{
    block::Block,
    codec::decode::{decode_block, DecodeConfig},
    debug::draw_debug,
    format::{header::Header, ser::Source},
    frame::Frame,
};
use log::{info, warn};
use std::io::{BufReader, BufWriter};

#[derive(Parser)]
#[clap(about, version)]
pub struct DecodeArgs {
    #[arg(long)]
    debug: bool,
}

fn main() -> anyhow::Result<()> {
    env_logger::init_from_env("LOG");
    let args = DecodeArgs::parse();

    let mut input = BufReader::new(std::io::stdin());
    let mut output = BufWriter::new(std::io::stdout());

    let header = input.get::<Header>().context("reading header")?;
    info!("{header:?}");
    if header.resolution.x * header.resolution.y > 100_000_000 {
        warn!("resolution is quite big. video is likely corrupt.");
    }
    let size = header.resolution;

    let config = DecodeConfig {};

    let progress_bar = ProgressBar::new(header.frame_count as u64);

    let mut prev = Frame::new(size);
    for i in 0..header.frame_count {
        info!("decode frame {i}");

        let block = Block::read(&mut input, size).context("reading encoded frame")?;
        let mut frame = Frame::new(size);

        decode_block(&block, frame.view_mut(), prev.view(), &config);

        progress_bar.inc(1);

        if args.debug {
            let mut f2 = frame.clone();
            draw_debug(&block, f2.view_mut());
            f2.write(&mut output).context("writing raw frame")?;
        } else {
            frame.write(&mut output).context("writing raw frame")?;
        }

        prev = frame;
    }
    drop(input);
    Ok(())
}
