use crate::{
    block::AdvancedReference,
    format::ser::map_scalar8,
    frame::Frame,
    helpers::{matrix::Mat2, pixel::Pixel, vector::Vec2},
    view::View,
};

#[derive(Debug, Clone)]
pub struct Sampler<'a> {
    pub view: View<&'a Frame>,
    pub halfsize: Vec2<f32>,

    pub translation: Vec2<f32>,
    pub transform: Mat2<f32>,

    pub value_scale: f32,
}

impl<'a> Sampler<'a> {
    #[inline]
    pub fn sample(&self, p: Vec2<f32>) -> Pixel {
        self.view
            .sample(self.translation + self.transform.transform(p - self.halfsize) + self.halfsize)
            .scale(self.value_scale)
    }
    pub fn from_refblock(
        view: View<&'a Frame>,
        AdvancedReference {
            translation,
            transform,
            value_scale,
        }: &AdvancedReference,
    ) -> Self {
        Self {
            transform: Mat2 {
                a: map_scalar8(transform.a),
                b: map_scalar8(transform.b),
                c: map_scalar8(transform.c),
                d: map_scalar8(transform.d),
            },
            halfsize: Into::<Vec2<f32>>::into(view.size).scale(0.5),
            translation: Vec2 {
                x: map_scalar8(translation.x),
                y: map_scalar8(translation.y),
            },
            value_scale: 1.05f32.powf(*value_scale as f32),
            view,
        }
    }
}
