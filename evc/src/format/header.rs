use crate::{
    format::ser::{Ser, Sink, Source},
    helpers::vector::Vec2,
};

#[derive(Debug, Clone, PartialEq, Copy)]
pub struct Header {
    pub resolution: Vec2<isize>,
    pub frame_count: usize,
}

pub const MAGIC: [u8; 4] = [0x5eu8, 0xb1u8, 0xc3u8, 0x08u8];

impl Ser for Header {
    fn write(&self, sink: &mut impl std::io::Write) -> anyhow::Result<()> {
        sink.put(MAGIC)?;
        sink.put((Into::<Vec2<u16>>::into(self.resolution), self.frame_count))?;
        Ok(())
    }

    fn read(source: &mut impl std::io::Read) -> anyhow::Result<Self> {
        assert_eq!(source.get::<[u8; 4]>()?, MAGIC);
        let (resolution, frame_count): (Vec2<u16>, usize) = source.get()?;
        Ok(Self {
            resolution: resolution.into(),
            frame_count,
        })
    }
}
