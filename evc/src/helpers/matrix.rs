use crate::helpers::vector::Vec2;

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Mat2<T> {
    pub a: T,
    pub b: T,
    pub c: T,
    pub d: T,
}

impl<T: std::ops::Mul<Output = T> + std::ops::Add<Output = T> + Copy> Mat2<T> {
    #[inline]
    pub fn transform(&self, v: Vec2<T>) -> Vec2<T> {
        Vec2 {
            x: self.a * v.x + self.b * v.y,
            y: self.c * v.x + self.d * v.y,
        }
    }
}

impl<T: std::ops::Mul<Output = T> + std::ops::Add<Output = T> + Copy> std::ops::Mul for Mat2<T> {
    type Output = Mat2<T>;
    #[inline]
    fn mul(self, rhs: Mat2<T>) -> Mat2<T> {
        let (x, y) = (self, rhs);
        Mat2 {
            a: x.a * y.a + x.b * y.c,
            b: x.a * y.b + x.b * y.d,
            c: x.c * y.a + x.d * y.c,
            d: x.c * y.b + x.d * y.d,
        }
    }
}
