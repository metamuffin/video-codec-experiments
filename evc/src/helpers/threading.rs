use std::{
    sync::atomic::{AtomicUsize, Ordering},
    thread,
};

static THREADS_RUNNING: AtomicUsize = AtomicUsize::new(0);

pub fn both_par<F1, F2, O1, O2>(f1: F1, f2: F2, max_threads: usize) -> (O1, O2)
where
    F1: FnOnce() -> O1 + Send + 'static,
    O1: Send + 'static,
    F2: FnOnce() -> O2,
{
    if THREADS_RUNNING.load(Ordering::Relaxed) < max_threads {
        THREADS_RUNNING.fetch_add(1, Ordering::Relaxed);

        let o1h = thread::spawn(move || f1());
        let o2 = f2();
        let o1 = o1h.join().unwrap();

        THREADS_RUNNING.fetch_sub(1, Ordering::Relaxed);
        (o1, o2)
    } else {
        (f1(), f2())
    }
}
