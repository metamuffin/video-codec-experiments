use super::compress::lit_decompress;
use crate::{block::Block, frame::Frame, refsampler::Sampler, view::View};

pub struct DecodeConfig {}

pub fn decode_block(
    block: &Block,
    mut target: View<&mut Frame>,
    prev: View<&Frame>,
    config: &DecodeConfig,
) {
    match &block {
        Block::Literal(pixels) => target.set_pixels(pixels),
        Block::Split(box [a, b]) => {
            let [a, b] = unsafe { std::mem::transmute::<_, [&'static Block; 2]>([a, b]) };
            let [at, bt] = unsafe {
                std::mem::transmute::<_, [View<&'static mut Frame>; 2]>(target.split_mut_unsafe())
            };
            let [ap, bp] =
                unsafe { std::mem::transmute::<_, [View<&'static Frame>; 2]>(prev.split()) };
            let config = unsafe { std::mem::transmute::<_, &'static DecodeConfig>(config) };

            rayon::join(
                move || decode_block(a, at, ap, config),
                move || decode_block(b, bt, bp, config),
            );
        }
        Block::CompressedLiteral(data) => {
            lit_decompress(&data, target);
        }
        Block::Reference { translation } => target.copy_from(&prev.offset(*translation)),
        Block::AdvancedReference(r) => target.copy_from_sampler(&Sampler::from_refblock(prev, r)),
    }
}
