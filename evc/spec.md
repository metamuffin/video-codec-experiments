# Low-Efficiency Video Codec

The codec compresses image sequences by avoiding redundancies by reusing parts
of previous frames. A frame is one block that is then subdivided further as
needed.

## Literal-Block

Stores the pixels' RGB values as is.

## Split-Block

Delegates to two sub-blocks. The block is split orthorgonally on the longest
axis. If needed, the left/top block is rounded down and the right/bottom.

## Reference-Block

Indicates that parts of the last frame are reused. The data reused is at the
position of this block in the last frame with the translation added.

## Advanced-Reference-Block

Like Reference-Block but instead translates by non-integer amount of pixels,
applying a 2x2 matrix from the center of the block and multiplying the color
value of each component, interpolating if necessary.

## Compressed-Literal-Block

_**JPEG? just DCT? idk**_

## File format

- magic bytes: `5e b1 c3 08`
- resolution: _`u16, u16`_
- frame count: _`u64`_
- frames (repeated [frame count]-times)
  - block kind: _`u8` (see tags below)_
  - block: _one of the following_
    - 0 **Literal-Block**
      - pixels: _`[[u8; 3]; (inferred)]`_ (stored line-by-line)
    - 1 **Compressed-Literal-Block**
      - length: _`u32`_
      - data: _`[[u8; length]`_
    - 2 **Split-Block**
      - sub-blocks: _`[block; 2]` (see above)_
    - 3 **Reference-Block**
      - translation: _`i8, i8`_
    - 4 **Advanced-Reference-Block**
      - translation: _`s8, s8`_ (translation)
      - transform: _`s8, s8, s8, s8`_ (2x2-matrix applied before sampling
        relative to the center)
      - value_scale: _`i8`_ (multiplication of each color component by $1.05^n$)

### Data Types

- _`u<n>`_: unsigned n-bit integer (little-endian)
- _`i<n>`_: signed n-bit integer using twos-complement (little-endian)
- _`s8`_: 8-bit scalar. When read as _`i8`_ represents a value of
  $\frac{x}{|x|} * \sqrt{2}^{|{x}| - 4}$ for $x \neq 0$, otherwise 0
- _`[<T>; <N>]`_: Array of T with length N
