struct Uniforms {
    x: f32,
    y: f32,
    rx: f32,
    ry: f32,
    r: f32,
    g: f32,
    b: f32
};

@group(0) @binding(0) var tex: texture_storage_2d<rgba8unorm, write>;
@group(0) @binding(1) var<uniform> uniforms: Uniforms;

@compute @workgroup_size(1)
fn main(@builtin(global_invocation_id) global_id: vec3<u32>) {
    let coords = global_id.xy;
    let kc = vec2<f32>(coords) - vec2(uniforms.x, uniforms.y);
    let d = length(kc / vec2(uniforms.rx, uniforms.ry));
    if d < 1.0 {
        textureStore(tex, vec2<i32>(coords), vec4<f32>(vec3<f32>(uniforms.r, uniforms.g, uniforms.b), 1.0));
    }
}
