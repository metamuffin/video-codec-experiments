use std::sync::Arc;

use wgpu::{Adapter, Device, Extent3d, ImageCopyTexture, Instance, Origin3d, Queue, Texture};

pub struct App {
    pub instance: Instance,
    pub device: Device,
    pub adapter: Adapter,
    pub queue: Queue,
}

impl App {
    pub async fn new() -> Arc<Self> {
        let instance = wgpu::Instance::new(wgpu::Backends::all());
        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions::default())
            .await
            .unwrap();
        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    label: None,
                    features: wgpu::Features::empty(),
                    limits: wgpu::Limits::downlevel_defaults(),
                },
                None,
            )
            .await
            .unwrap();
        Arc::new(Self {
            adapter,
            device,
            instance,
            queue,
        })
    }

    pub fn copy_texture(&self, source: &Texture, destination: &Texture, size: Extent3d) {
        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });
        encoder.copy_texture_to_texture(
            ImageCopyTexture {
                aspect: wgpu::TextureAspect::All,
                mip_level: 0,
                origin: Origin3d::ZERO,
                texture: source,
            },
            ImageCopyTexture {
                aspect: wgpu::TextureAspect::All,
                mip_level: 0,
                origin: Origin3d::ZERO,
                texture: destination,
            },
            size,
        );
        self.queue.submit(Some(encoder.finish()));
    }
}
