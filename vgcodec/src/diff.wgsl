@group(0) @binding(0) 
var tex_a: texture_2d<f32>;
@group(0) @binding(1) 
var tex_b: texture_2d<f32>;

@group(0) @binding(2)
var<storage, read_write> exp: atomic<u32>;

@compute @workgroup_size(1)
fn main(@builtin(global_invocation_id) global_id: vec3<u32>) {
    var col_a = textureLoad(tex_a, vec2(i32(global_id.x), i32(global_id.y)), 0);
    var col_b = textureLoad(tex_b, vec2(i32(global_id.x), i32(global_id.y)), 0);
    var diff = length(col_a - col_b);
    atomicAdd(&exp, u32(diff * 1000.0));
}

