pub mod app;
pub mod approximate;
pub mod diff;
pub mod export;
pub mod helper;
pub mod paint;

use app::App;
use approximate::Approximator;
use clap::Parser;
use helper::write_texture;
use log::info;
use wgpu::TextureUsages;

fn main() {
    env_logger::init_from_env("LOG");
    pollster::block_on(run());
}

#[derive(Parser)]
#[clap(about)]
struct Args {
    #[clap(short = 'I', long)]
    iterations: usize,
    #[clap(short = 'o', long)]
    outfile: String,
    infile: String,
}

async fn run() {
    let args = Args::parse();
    let app = app::App::new().await;

    let App { device, queue, .. } = app.as_ref();

    let img_target = image::open(&args.infile).unwrap().into_rgba8();

    let size = wgpu::Extent3d {
        width: img_target.width(),
        height: img_target.height(),
        depth_or_array_layers: 1,
    };

    let tex_target = device.create_texture(&wgpu::TextureDescriptor {
        size,
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format: wgpu::TextureFormat::Rgba8Unorm,
        usage: TextureUsages::COPY_DST | TextureUsages::TEXTURE_BINDING | TextureUsages::COPY_SRC,
        label: None,
    });

    let img_raw = img_target.into_raw();
    info!("{}", img_raw.len());
    write_texture(queue, &tex_target, &img_raw, size);

    let mut a = Approximator::new(&app, tex_target, size);
    a.run(args.iterations, &args.outfile).await;
}
