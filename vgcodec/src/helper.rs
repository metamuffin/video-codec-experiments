use log::info;
use wgpu::{Extent3d, Queue, Texture};

pub fn write_texture(queue: &Queue, target: &Texture, data: &[u8], size: Extent3d) {
    info!("uploading texture {size:?} ({} bytes)", data.len());

    let bytes_per_pixel = std::mem::size_of::<u32>() as u32;
    let unpadded_bytes_per_row = size.width * bytes_per_pixel;
    let align = wgpu::COPY_BYTES_PER_ROW_ALIGNMENT;
    let padded_bytes_per_row_padding = (align - unpadded_bytes_per_row % align) % align;
    let padded_bytes_per_row = unpadded_bytes_per_row + padded_bytes_per_row_padding;

    let mut padded = vec![];
    for y in 0..(size.height as usize) {
        for x in 0..(size.width as usize) {
            for c in 0..4 {
                padded.push(data[c + x * 4 + y * 4 * size.width as usize])
            }
        }
        for _ in 0..padded_bytes_per_row_padding {
            padded.push(0)
        }
    }

    queue.write_texture(
        wgpu::ImageCopyTexture {
            texture: &target,
            mip_level: 0,
            origin: wgpu::Origin3d::ZERO,
            aspect: wgpu::TextureAspect::All,
        },
        &padded,
        wgpu::ImageDataLayout {
            offset: 0,
            bytes_per_row: Some(std::num::NonZeroU32::try_from(padded_bytes_per_row).unwrap()),
            rows_per_image: None,
        },
        size,
    );
}
