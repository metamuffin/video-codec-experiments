pub mod motion;

use framework::{CodecMode, Framework};
use log::{debug, info};
use motion::{dec::MotionDecoder, enc::MotionEncoder, CommonBuffers, Params};
use pollster::FutureExt;
use std::{
    io::{stdin, stdout, ErrorKind, Read, Write},
    process::exit,
    time::Instant,
};
use wgpu::{
    Backends, DeviceDescriptor, Extent3d, Features, Instance, InstanceDescriptor, Limits,
    MaintainBase, PowerPreference, RequestAdapterOptions,
};

use crate::motion::{debug::MotionDebugger, RoundParams};

fn main() {
    env_logger::init_from_env("LOG");
    info!("allocating some stuff");
    let t = Instant::now();

    let instance = Instance::new(InstanceDescriptor {
        backends: Backends::all(),
        ..Default::default()
    });

    let adapter = instance
        .request_adapter(&RequestAdapterOptions {
            power_preference: PowerPreference::HighPerformance,
            force_fallback_adapter: false,
            compatible_surface: None,
        })
        .block_on()
        .unwrap();

    let (device, queue) = adapter
        .request_device(
            &DeviceDescriptor {
                label: None,
                required_features: Features::empty(),
                required_limits: Limits::default(),
            },
            None,
        )
        .block_on()
        .unwrap();

    let (mut framework, fparams) = Framework::init();

    let (width, height) = (fparams.width, fparams.height);
    let bsize = 8;
    let params = Params {
        width,
        height,
        extent: Extent3d {
            width: width as u32,
            height: height as u32,
            depth_or_array_layers: 1,
        },
        blocks_x: width / bsize,
        blocks_y: height / bsize,
        block_width: bsize,
        block_height: bsize,
        blocks: (width / bsize) * (height / bsize),
        init_debug: true,
    };

    let bufs = CommonBuffers::create(&device, &params);
    let menc = MotionEncoder::create(&device, &params, &bufs);
    let mdec = MotionDecoder::create(&device, &params, &bufs);
    let mdeb = MotionDebugger::create(&device, &params, &bufs);

    let mut buffer = vec![0u8; width * height * 4];

    menc.write_uniforms(&queue);
    mdec.write_uniforms(&queue);
    mdeb.write_uniforms(&queue);

    info!("ready (setup took {:?})", t.elapsed());

    let mut i = 0;
    loop {
        let rp = RoundParams {
            swap: i % 2,
            debug: fparams.debug == 2,
            preview: fparams.debug > 0,
        };
        debug!("{params:?} {rp:?}");
        debug!("read");
        match stdin().read_exact(&mut buffer) {
            Ok(_) => (),
            Err(e) if e.kind() == ErrorKind::UnexpectedEof => {
                break;
            }
            Err(e) => Err(e).unwrap(),
        }

        framework.next_frame_manual();

        debug!("upload");
        bufs.upload_texture(&queue, &params, &rp, &buffer);

        debug!("compute");
        let mut encoder = device.create_command_encoder(&Default::default());

        if let CodecMode::Encode = fparams.mode {
            menc.pass(&mut encoder, &params, &rp);
        }
        mdec.pass(&mut encoder, &params, &rp);
        if rp.debug {
            mdeb.pass(&mut encoder, &params, &rp);
        }
        if rp.preview {
            bufs.prepare_texture_download(&mut encoder, &params, &rp);
        }

        queue.submit(Some(encoder.finish()));
        device.poll(MaintainBase::Wait);

        debug!("download");
        bufs.download_texture(&device, &mut buffer);

        framework.encode_done(&[]);
        debug!("write");
        stdout().write_all(&buffer).unwrap();
        i += 1;
    }
    eprintln!("done");
    exit(0);
}
