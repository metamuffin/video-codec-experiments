
struct Params {
    block_size: vec2<i32>,
    offsets_stride: u32
}

struct BlockOffset {
    score: f32,
    offset: vec2<i32>,
    tint: vec3<f32>,
}

@group(0) @binding(0) var<uniform> params: Params;
@group(0) @binding(1) var<storage, read> offsets: array<BlockOffset>;
@group(0) @binding(2) var next: texture_storage_2d<rgba8unorm, write>;
@group(0) @binding(3) var prev: texture_2d<f32>;

@compute @workgroup_size(1) fn main(@builtin(global_invocation_id) global_id: vec3<u32>) {
    let uv = vec2<i32>(global_id.xy) * params.block_size;

    let bl = offsets[global_id.x + global_id.y * params.offsets_stride];

    for (var x = 0; x < params.block_size.x; x++) {
    for (var y = 0; y < params.block_size.y; y++) {
        let base = uv+vec2(x,y);
        let col = textureLoad(prev, base+bl.offset, 0)+vec4(bl.tint,1.);
        textureStore(next, base, col);
    }}
}


