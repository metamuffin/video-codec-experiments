

struct Params {
    block_size: vec2<i32>,
    offsets_stride: u32
}

struct BlockOffset {
    score: f32,
    offset: vec2<i32>,
    tint: vec3<f32>,
}

@group(0) @binding(0) var<uniform> params: Params;
@group(0) @binding(1) var<storage, read> offsets: array<BlockOffset>;
@group(0) @binding(2) var next: texture_2d<f32>;
@group(0) @binding(3) var prev: texture_2d<f32>;
@group(0) @binding(4) var out: texture_storage_2d<rgba8unorm, write>;

@compute @workgroup_size(1)fn main(@builtin(global_invocation_id) global_id: vec3<u32>) {
    let uv = vec2<i32>(global_id.xy) * params.block_size;

    let bl = offsets[global_id.x + global_id.y * params.offsets_stride];

    for (var x = 0; x < params.block_size.x; x++) {
        for (var y = 0; y < params.block_size.y; y++) {
            let base = uv + vec2(x, y);
            let col = vec4(colormap_vec(vec2<f32>(bl.offset) * 0.08),1.);
            textureStore(out, base, col);
        }
    }

    // for (var x = 0; x < params.block_size.x; x++) {
    //     for (var y = 0; y < params.block_size.y; y++) {
    //         let base = uv + vec2(x, y);
    //         let col_dec = textureLoad(prev, base + bl.offset, 0) + vec4(bl.tint, 1.);
    //         let col_orig = textureLoad(next, base, 0);

    //         let col = vec4( saturate((0.5) + (col_orig.rgb - col_dec.rgb) * 100.), 1.0);

    //         textureStore(out, base, col);
    //     }
    // }
}
 

fn colormap_vec(v: vec2<f32>) -> vec3<f32> {
    return vec3(v.y, v.x - 0.5 * v.y, -v.x - 0.5 * v.y);
}

