use bv1::{
    decode,
    encode::{encode, EncodeConfig},
    P2,
};
use clap::{Parser, Subcommand};
use std::io::{stdin, stdout};

#[derive(Parser)]
#[clap(about, version)]
struct Args {
    // Width of the video signal
    #[arg(short = 'W', long)]
    width: u16,
    // Height of the video signal
    #[arg(short = 'H', long)]
    height: u16,
    #[clap(subcommand)]
    action: Action,
}

#[derive(Clone, Subcommand)]
enum Action {
    // Compress video
    Encode {
        #[arg(short, long, default_value_t = 800)]
        max_block_size: usize,
        #[arg(short, long, default_value_t = 10_000)]
        attention_split: u32,
        #[arg(short, long, default_value_t = 10.)]
        threshold: f32,
        #[arg(short, long, default_value_t = 10)]
        keyframe_interval: usize,
    },
    // Decompress video
    Decode {
        #[arg(short, long)]
        debug: bool,
    },
}

fn main() {
    let args = Args::parse();

    let size = P2 {
        x: args.width as i32,
        y: args.height as i32,
    };
    match args.action {
        Action::Encode {
            max_block_size,
            threshold,
            attention_split,
            keyframe_interval,
        } => {
            let config = EncodeConfig {
                min_block_size: 16,
                motion_split_f: 2.,
                threshold,
                max_block_size,
                attention_split,
                keyframe_interval,
            };

            encode(config, size, stdin(), stdout()).unwrap();
        }
        Action::Decode { debug } => {
            decode(size, debug, stdin(), stdout()).unwrap();
        }
    }
}
