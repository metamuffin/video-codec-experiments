use bv1::{encode::diff, Frame, Ref, View, P2};
use std::time::Instant;

fn measure(f: impl FnOnce()) {
    let t1 = Instant::now();
    f();
    let t2 = Instant::now();
    eprintln!("took {:?}", (t2 - t1));
}

fn main() {
    let size = P2 { x: 2000, y: 2000 };
    let f1 = Frame::new(size);
    let f2 = Frame::new(size);
    measure(|| {
        diff([&f1, &f2], View::all(size), Ref::default());
    });
}

// #[test]
// fn bench_fast_diff() {
//     let size = P2 { x: 2000, y: 2000 };
//     let f1 = Frame::new(size);
//     let f2 = Frame::new(size);
//     measure(|| {
//         diff_fast([&f1, &f2], View::all(size), Ref::default());
//     });
// }
