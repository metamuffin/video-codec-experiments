#[cfg(test)]
mod test {

    use bv1::huff::{read_huff, write_huff, BitIO};
    use std::io::Cursor;

    #[test]
    fn test_bitio() {
        let mut buf = Vec::<u8>::new();

        {
            let mut b = BitIO::new(Cursor::new(&mut buf));
            b.wbit(true).unwrap();
            b.wbit(true).unwrap();
            b.wbit(true).unwrap();
            b.wbit(true).unwrap();
            b.wbit(false).unwrap();
            b.wbit(true).unwrap();
            b.wbit(true).unwrap();
            b.wbit(true).unwrap();
            b.wbit(true).unwrap();
            b.wbyte(0xff).unwrap();
            b.flush().unwrap();
        }
        {
            let mut b = BitIO::new(Cursor::new(&mut buf));
            for _ in 0..17 {
                let _v = b.rbit().unwrap();
                // eprintln!("{:?}", _v)
            }
        }
    }

    #[test]
    fn test_huff() {
        let a = vec![1, 2, 3, 4, 5, 1, 3, 6, 3, 2, 4, 6, 7, 4, 3, 2, 1, 3, 4];
        let mut b = vec![];

        let mut buf = Vec::<u8>::new();
        write_huff(&a, &mut Cursor::new(&mut buf)).unwrap();
        read_huff(&mut Cursor::new(&mut buf), &mut b).unwrap();
        
        assert_eq!(a, b)
    }
}

fn main() {}
