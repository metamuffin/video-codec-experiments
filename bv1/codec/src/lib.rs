#![feature(portable_simd)]

pub mod debug;
pub mod decode;
pub mod encode;
pub mod frameio;
pub mod huff;
pub mod impls;
pub mod serialize;
pub mod split;

pub type PixelValue = i16;

pub use decode::{decode, Decoder};
pub use encode::encode;

#[derive(Debug, Clone, Copy, Default, PartialEq, Eq)]
pub struct Pixel {
    pub r: PixelValue,
    pub g: PixelValue,
    pub b: PixelValue,
}

#[derive(Debug, Clone, Copy, Default, PartialEq, Eq)]
pub struct P2 {
    pub x: i32,
    pub y: i32,
}

pub struct Frame {
    pub size: P2,
    pub pixels: Vec<Pixel>,
}

#[derive(Debug, Clone, Copy)]
pub struct View {
    pub a: P2,
    pub b: P2,
}

#[derive(Debug, Clone)]
pub enum Block {
    Split(Box<Block>, Box<Block>),
    Lit(Vec<Pixel>),
    Ref(Ref),
}

#[derive(Debug, Clone, Copy, Default)]
pub struct Ref {
    pub pos_off: P2,
    pub color_off: Pixel,
}
