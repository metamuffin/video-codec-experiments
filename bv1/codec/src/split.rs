use crate::{View, P2};

pub fn split(view: View) -> [View; 2] {
    let s = view.size();
    if s.x > s.y {
        let mid_x = (view.a.x + view.b.x) / 2;
        [
            View {
                a: view.a,
                b: P2 {
                    x: mid_x,
                    y: view.b.y,
                },
            },
            View {
                a: P2 {
                    x: mid_x,
                    y: view.a.y,
                },
                b: view.b,
            },
        ]
    } else {
        let mid_y = (view.a.y + view.b.y) / 2;
        [
            View {
                a: view.a,
                b: P2 {
                    x: view.b.x,
                    y: mid_y,
                },
            },
            View {
                a: P2 {
                    x: view.a.x,
                    y: mid_y,
                },
                b: view.b,
            },
        ]
    }
}
