use crate::{Frame, Pixel, PixelValue, P2};
use std::io::{Read, Result, Write};

impl Frame {
    pub fn read(inp: &mut impl Read, size: P2) -> Result<Frame> {
        let mut f = Frame::new(size);

        for y in 0..size.y {
            for x in 0..size.x {
                let mut cc = [0u8; 3];
                inp.read_exact(&mut cc)?;
                f[P2 { x, y }] = Pixel {
                    r: cc[0] as PixelValue,
                    g: cc[1] as PixelValue,
                    b: cc[2] as PixelValue,
                };
            }
        }
        Ok(f)
    }

    pub fn write(out: &mut impl Write, frame: &Frame) -> Result<()> {
        for y in 0..frame.size.y {
            for x in 0..frame.size.x {
                let p = frame[P2 { x, y }];
                let mut cc = [0u8; 3];
                cc[0] = p.r.clamp(0, 255) as u8;
                cc[1] = p.g.clamp(0, 255) as u8;
                cc[2] = p.b.clamp(0, 255) as u8;
                out.write_all(&mut cc)?;
            }
        }
        Ok(())
    }
}
