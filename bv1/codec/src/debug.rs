use crate::{split::split, Block, Frame, Pixel, View, P2};

pub fn draw_debug(frame: &mut Frame, view: View, block: &Block) {
    match block {
        Block::Lit(_) => rect(frame, view, Pixel::GREEN),
        Block::Split(a, b) => {
            let [av, bv] = split(view);
            draw_debug(frame, av, &a);
            draw_debug(frame, bv, &b);
        }
        Block::Ref(r) => {
            let v = View {
                a: view.a,
                b: view.a + P2 { x: 2, y: 2 },
            };
            if r.pos_off != P2::ZERO {
                fill_rect(frame, v + P2 { x: 0, y: 0 }, Pixel::BLUE)
            }
            if r.color_off != Pixel::BLACK {
                fill_rect(frame, v + P2 { x: 2, y: 0 }, Pixel::RED)
            }
        }
    }
}

fn rect(frame: &mut Frame, view: View, color: Pixel) {
    for x in view.a.x..view.b.x {
        frame[P2 { x, y: view.a.y }] = color;
        frame[P2 { x, y: view.b.y - 1 }] = color;
    }
    for y in view.a.y..view.b.y {
        frame[P2 { y, x: view.a.x }] = color;
        frame[P2 { y, x: view.b.x - 1 }] = color;
    }
}
fn fill_rect(frame: &mut Frame, view: View, color: Pixel) {
    for y in view.a.y..view.b.y {
        for x in view.a.x..view.b.x {
            frame[P2 { x, y }] = color;
        }
    }
}
