/// <reference lib="dom" />

import init, { decode_frame, decode_init } from "./codec_web.js"
console.log("init wasm");
await init()
console.log("done");

index("..")

async function index(url: string) {
    const res = await fetch(url)
    if (!res.ok) throw new Error("not ok");
    document.body.innerHTML = await res.text();
    const h1 = document.createElement("h1")
    h1.textContent = "bv1 web player"
    document.body.prepend(h1)
    document.body.querySelectorAll("a").forEach(e => {
        const u = url + "/" + e.textContent
        e.onclick = ev => {
            ev.preventDefault()
            if (!u.endsWith(".bv1")) return alert("thats not bv1!")
            document.body.innerHTML = ""
            play(u.toString())
        }
    })
}

async function play(url: string) {
    decode_init(1920, 1080)
    document.body.innerText = "downloading…"
    const res = await fetch(url)
    if (!res.ok) throw new Error("not ok");
    let buf: Uint8Array | undefined = new Uint8Array(await res.arrayBuffer())
    document.body.innerText = ""

    const canvas = document.createElement("canvas")
    canvas.width = 1920
    canvas.height = 1080
    document.body.append(canvas)
    document.body.style.backgroundColor = "#111"
    const ctx = canvas.getContext("2d")!
    let debug = false;

    document.body.addEventListener("keydown", ev => {
        if (ev.code == "KeyD") debug = !debug;
    })

    setInterval(() => {
        const frame = decode_frame(buf ?? new Uint8Array(), debug);
        buf = undefined

        const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        const data = imageData.data;

        for (let y = 0; y < 1080; y++) {
            for (let x = 0; x < 1920; x++) {
                const ti = (x + y * 1920) * 4;
                const si = (x + y * 1920) * 3;
                data[ti + 0] = frame[si + 0]
                data[ti + 1] = frame[si + 1]
                data[ti + 2] = frame[si + 2]
                data[ti + 3] = 255
            }
        }

        ctx.putImageData(imageData, 0, 0);

    }, 1000 / 30)
}
