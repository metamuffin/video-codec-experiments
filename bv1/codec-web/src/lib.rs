use bv1::{Decoder, Frame, P2};
use std::{collections::VecDeque, sync::RwLock};
use wasm_bindgen::prelude::*;

static DECODER: RwLock<Option<State>> = RwLock::new(None);

struct State {
    buffer: VecDeque<u8>,
    frame: Frame,
    decoder: Decoder,
}

// #[wasm_bindgen]
// extern "C" {
//     #[wasm_bindgen(js_namespace = console)]
//     fn log(s: &str);
//     #[wasm_bindgen(js_namespace = console, js_name = "log")]
//     fn logs(s: String);
// }

#[wasm_bindgen(start)]
fn panic_init() {
    std::panic::set_hook(Box::new(console_error_panic_hook::hook));
}

#[wasm_bindgen]
pub fn decode_init(width: i32, height: i32) {
    let size = P2 {
        x: width,
        y: height,
    };
    *DECODER.write().unwrap() = Some(State {
        frame: Frame::new(size),
        decoder: Decoder::new(size),
        buffer: VecDeque::new(),
    });
}

#[wasm_bindgen]
pub fn decode_frame(buf: &[u8], debug: bool) -> Vec<u8> {
    let mut arr = Vec::new();
    let mut g = DECODER.write().unwrap();
    let state = g.as_mut().unwrap();
    state.buffer.extend(buf.iter());

    state
        .decoder
        .decode_frame(&mut state.buffer, &mut state.frame, debug)
        .unwrap();

    for y in 0..state.frame.size.y {
        for x in 0..state.frame.size.x {
            arr.push(state.frame[P2 { x, y }].r.clamp(0, 255) as u8);
            arr.push(state.frame[P2 { x, y }].g.clamp(0, 255) as u8);
            arr.push(state.frame[P2 { x, y }].b.clamp(0, 255) as u8);
        }
    }

    arr
}
