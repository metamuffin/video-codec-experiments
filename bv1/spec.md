# BOMedia Video 1

The codec compresses image sequences by avoiding redundancies by reusing parts
of previous frames. A frame is one block that is then subdivided further as
needed.

## Literal-Block

Stores the pixels' RGB values as is.

## Split-Block

Delegates to two sub-blocks. The block is split orthorgonally on the longest
axis. If needed, the left/top block is rounded down and the right/bottom rounded
up.

## Reference-Block

Indicates that parts of the last frame are reused. The data reused is at the
position of this block in the last frame with the translation added.

## File format

- magic bytes: `5e b1 c3 08`
- resolution: _`u16, u16`_
- frame count: _`u64`_
- frames (repeated [frame count]-times): Frame (see below)

## Frame format

- block kind: _`u8` (see tags below)_
- block: _one of the following_
  - 0 **Split-Block**
    - sub-blocks: _`[block; 2]` (see above)_
  - 1 **Literal-Block**
    - pixels: _`[[u8; 3]; (inferred)]`_ (stored line-by-line)
  - 2 **Reference-Block**
    - position_offset: _`[i8; 2]`_
    - color_offset: _`[i16; 3]`_

### Data Types

- _`u<n>`_: unsigned n-bit integer (little-endian)
- _`i<n>`_: signed n-bit integer using twos-complement (little-endian)
- _`[<T>; <N>]`_: Array of T with length N
